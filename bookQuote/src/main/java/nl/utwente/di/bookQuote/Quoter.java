package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    public Double Quoter(double i) {
        return i*9/5+32;
    }
    public double getBookPrice(String isbn){
        double i=Double.parseDouble(isbn);
        return Quoter(i);
    }
}
